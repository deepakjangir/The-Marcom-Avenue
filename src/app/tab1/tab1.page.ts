import { Component } from '@angular/core';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  recentWatch=[{
    title:"Tattad Tattad - Goliyon ki Rasleela",
    tags:{genre:"Bollywood",level:"Beginner",type:"Hiphop"},
    completed:85,
    time:"00:24:07",
    thumb:"assets/video-images/thumb1.png"
  },
  {
    title:"Tattad Tattad - Goliyon ki Rasleela",
    tags:{genre:"Bollywood",level:"Beginner",type:"Hiphop"},
    completed:80,
    time:"00:24:07",
    thumb:"assets/video-images/thumb2.png"
  },
  {
    title:"Tattad Tattad - Goliyon ki Rasleela",
    tags:{genre:"Bollywood",level:"Beginner",type:"Hiphop"},
    completed:55,
    time:"00:24:07",
    thumb:"assets/video-images/thumb3.png"
  }];

  allCourses=[{
    title:"Related Courses",
    likes:5000,
    views:500,
    ratings:4,
    thumb:"assets/video-images/thumb1.png"
  },
  {
    title:"Related Courses",
    likes:5000,
    views:500,
    ratings:2,
    thumb:"assets/video-images/thumb1.png"
  },
  {
    title:"Related Courses",
    likes:5000,
    views:500,
    ratings:3,
    thumb:"assets/video-images/thumb1.png"
  }]

  selectedSeg="explore";
  selectedFilter="course";
  constructor() {}

}
