import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { IonRouterOutlet, NavController, Platform, ToastController } from '@ionic/angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>
  @ViewChild(IonRouterOutlet, { static: false }) routerOutlet: IonRouterOutlet;
  lastTimeBackPress = 0;
  timePeriodToExit = 2000;
  constructor(
    private platform: Platform,
    private router: Router,
    private navCtrl: NavController,
    private toastCtrl: ToastController,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen
  ) { 
    this.initializeApp();
    this.backButtonEvent();
  }


  initializeApp() {
    this.platform.ready().then(() => {
      this.splashScreen.hide();
      this.statusBar.backgroundColorByHexString("#fcfcfc");
      this.statusBar.styleDefault();
      // this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    });
  }
  backButtonEvent() {
    this.platform.backButton.subscribeWithPriority(0, () => {

      if (this.routerOutlet && this.routerOutlet.canGoBack()) {
        this.routerOutlet.pop();
      } else if (this.router.url === '/home') {
        this.navCtrl.navigateRoot('initial-screen');
        return;
      }
      else if (this.router.url === '/tabs/tab1' || this.router.url === '/initial-screen') {
        if (new Date().getTime() - this.lastTimeBackPress >= this.timePeriodToExit) {
          this.lastTimeBackPress = new Date().getTime();
          this.presentToast("Press again to exit!", '', 'bottom', 2000);
        } else {
          navigator['app'].exitApp();
        }
      } else {
        this.navCtrl.navigateRoot('tabs');
      }
    });
  }

  async presentToast(msg, color, pos, dur?) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: dur || 2000,
      position: pos,
      color: color
    });
    toast.present();
  }
}
