import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VideojsPlayerPage } from './videojs-player.page';

const routes: Routes = [
  {
    path: '',
    component: VideojsPlayerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VideojsPlayerPageRoutingModule {}
