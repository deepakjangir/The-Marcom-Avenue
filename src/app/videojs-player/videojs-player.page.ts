import { Component, OnInit } from '@angular/core';
import videojs from 'video.js';
import 'videojs-seek-buttons';
import 'videojs-http-source-selector';
import 'videojs-contrib-quality-levels';
@Component({
  selector: 'app-videojs-player',
  templateUrl: './videojs-player.page.html',
  styleUrls: ['./videojs-player.page.scss'],
})
export class VideojsPlayerPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
