import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VideojsPlayerPageRoutingModule } from './videojs-player-routing.module';

import { VideojsPlayerPage } from './videojs-player.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VideojsPlayerPageRoutingModule
  ],
  declarations: [VideojsPlayerPage]
})
export class VideojsPlayerPageModule {}
